function loadMenu(obj) {
	if(obj.data("loaded")=="true")return;
	$.get("menu/"+obj.data("pid"), function(data) {
		if(data.success){
			data=data.data;
			for(var i=0;i<data.length;i++){
				var m=data[i];
				var html=("<li "+(m.isparent?"class='treeview'":"")+">");
				
				html+=("<a href='javascript:void(0);' "+(_.isEmpty(m.uri)?"":"data-uri='"+m.uri+"'")+" data-target='"+m.target+"' onclick='loadWorker(this);'>");
				html+=("<i class='fa "+m.icon+"'></i><span>"+m.name+"</span>");
				if(m.isparent)html+="<span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span>";
				html+="</a>";
				if(m.isparent)html+=("<ul class='treeview-menu' data-pid='"+m.id+"'></ul>");
				html+="</li>";
				obj.append(html);
			}
			obj.data("loaded","true");
		}else{
			showMsg("系统异常：" +(""==data?"未知错误":data.msg),"error");
		}
	}).error(function(e) {
		waitLoading(false);
		showMsg("系统异常：" + e.statusText,"error");
	});
};

function loadWorker(srcObj) {
	var $this = $(srcObj);
	var ul=$this.parent().find("ul.treeview-menu");
	if (ul.length>0) {
		loadMenu(ul);
	}
	var uri = $this.data("uri");
	if (_.isEmpty(uri))
		return;
	if("_blank"==$this.data("target")){
		window.open(uri,$this.text());
	}else{
		loadUri("worker",uri,function(){
			$(".content-header > h1").html($this.html());
		});
	}
};
function _initMainBtn(){
	$("#user_set_btn").click(function(){
		var buttons = [ {
				label : '保存',
				cssClass : 'btn-success btn-sm',
				icon : 'fa fa-check-square',
				action : function(dialog) {
					postValidatorForm("user-set-form", function(data) {
					});
				}
			}, {
				label : '取消',
				cssClass : 'btn-info btn-sm',
				icon : 'fa fa-window-close',
				action : function(dialog) {
					dialog.close();
				}
			} ];
			loadUriDialog("用户设置", "user/set", null,null,null, buttons);
	});
	$("#user_exit_btn").click(function(){
		confirmDialog("<strong class='text-danger'>确定退出系统吗？</strong>", function() {
			location.href="logout";
		});
	});	
};
function _loadNotice(){
	$.get("notice/portlet",function(data){
		var $li = $("li.notifications-menu");
		$li.find("span.label-warning").html(data.totalRow);
		$li.find("li.header > i").html(data.totalRow);
		var list = data.list;
		var $ul=$li.find("ul.menu");
		$ul.empty();
		for(var i=0;i<list.length;i++){
			var temp=list[i];
			$ul.append("<li><a href='javascript:void(0);' class='auto-btn auto-dialog' data-title='通知公告详情' data-uri='notice/get/"+temp.id+"'> <i class='fa fa-bell text-aqua'></i>"+temp.title+"</a> </li>");
		}
		new autoEventListener($li,false).initBtnEvent();
	});
};

function _loadMsg(){
	$.get("msg/portlet",function(data){
		var $li = $("li.messages-menu");
		$li.find("span.label-success").html(data.totalRow);
		$li.find("li.header > i").html(data.totalRow);
		var list = data.list;
		var $ul=$li.find("ul.menu");
		$ul.empty();
		for(var i=0;i<list.length;i++){
			var temp=list[i];
			$ul.append("<li><a href='javascript:void(0);' class='auto-btn auto-dialog' data-title='系统消息' data-uri='msg/get/"+temp.id+"'><div class='pull-left'><i class='fa fa-envelope-o text-aqua'></i></div><h4>"+temp.from_name+"<small><i class='fa fa-clock-o'></i>"+timeago(temp.ctime)+"</small></h4><p>"+temp.subject+"</p></a></li>");
		}
		new autoEventListener($li,false).initBtnEvent();
	});
};

function _loadTask(){
	$.get("notice/portlet",function(data){
		var $li = $("li.tasks-menu");
		$li.find("span.label-danger").html(data.totalRow);
		$li.find("li.header > i").html(data.totalRow);
		var list = data.list;
		var $ul=$li.find("ul.menu");
		$ul.empty();
		for(var i=0;i<list.length;i++){
			var temp=list[i];
			$ul.append("<li title='20%'><a href='javascript:void(0);' class='auto-btn auto-dialog' data-title='待办事项' data-uri='notice/get/"+temp.id+"'><h3>"+temp.title+"</h3> <div class='progress xs'><div class='progress-bar progress-bar-aqua' style='width: 20%' role='progressbar' aria-valuenow='20' aria-valuemin='0' aria-valuemax='100'><span class='sr-only'>20% Complete</span></div></div></a></li>");
		}
		new autoEventListener($li,false).initBtnEvent();
	});
};
function _gotop(){
    $(window).scroll(function(){
        if ($(this).scrollTop()>300) {
          $('.totop').slideDown();
        } else {
          $('.totop').slideUp();
        }
      });

      $('.totop a').click(function (e) {
        e.preventDefault();
        $('body,html').animate({scrollTop: 0}, 500);
      });
};
function _searchBtn(){
	$("#top-search-a").click(function(){
		_searchIndex(1);
	});
}
function _searchIndex(page){
	var kw = $("#top-search").val();
	if("" == kw){
		showMsg("请输入关键字","error");
		$("#top-search").focus();
		return;
	}
	postData("search/"+page, {keyword:kw}, null, function(data){
		$(".content-header > h1").html("<i class='fa fa-search'></i>全文检索");
		$("#worker").html(data);
	},"html");
}
$(document).ready(function() {
	_gotop();
	loadMenu($("#sidebar_ul"));//初始化菜单
	loadWorker($("#go_home_btn"));//默认桌面
	waitLoading(false);
	_initMainBtn();//初始化按钮
	_searchBtn();
	
	_loadMsg();//初始化邮件
	setInterval("_loadMsg()", 15*60*1000);//15分钟更新一次
	
	_loadNotice();//初始化公告
	setInterval("_loadNotice()", 15*60*1000);//15分钟更新一次
	
	_loadTask();//待办事项
	setInterval("_loadTask()", 20*60*1000);//20分钟更新一次
	
});