package com.yj.auto.core.base.online;

/**
 * 
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import cn.hutool.core.util.ArrayUtil;
import com.yj.auto.Constants;

public class SessionUserUtil {
	/**
	 * 设置当前用户，SessionUser
	 * 
	 * @param request
	 * @return
	 */
	public static void addSessionUser(HttpServletRequest request, SessionUser su) {
		HttpSession session = request.getSession();
		session.setAttribute(Constants.SYS_SESSION_USER, su);
		session.setAttribute(Constants.ONLINE_USER_LISTENER_KEY, new OnlineUserListener(su));
	}

	public static SessionUser addSessionUser(HttpServletRequest request, Integer userId, String userCode, String userName, Integer orgId, Integer[] roles) {
		SessionUser su = new SessionUser(userId, userCode, userName);
		su.setOrgId(orgId);
		su.setRoles(roles);
		addSessionUser(request, su);
		return su;
	}
	/**
	 * 得到当前用户，SessionUser
	 *
	 * @param request
	 * @return
	 */
	public static SessionUser getSessionUser(HttpServletRequest request) {
		return getSessionUser(request.getSession());
	}

	public static SessionUser getSessionUser(HttpSession session) {
		return (SessionUser) session.getAttribute(Constants.SYS_SESSION_USER);
	}

	/**
	 * 判断 当前用户是否属于指定角色
	 * 
	 * @param request
	 * @param roleId
	 *            角色ID
	 * @return
	 */
	public static boolean isRoleUser(HttpServletRequest request, Integer roleId) {
		SessionUser su = getSessionUser(request);
		if (null != su) {
			return su.isRoleUser(roleId);
		}
		return false;
	}

	/**
	 * 是否超级用户
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isSuperUser(HttpServletRequest request) {
		return isRoleUser(request, Constants.ROLE_ID_SUPER_USER);
	}

	/**
	 * 是否系统管理员
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isAdmin(HttpServletRequest request) {
		return isRoleUser(request, Constants.ROLE_ID_ADMIN_USER);
	}
	//
	// /**
	// * 移除SESSION
	// *
	// * @param request
	// */
	// public static void removeSessionUser(HttpServletRequest request) {
	// request.getSession().invalidate();
	// }
	//
	// public static String getUserCode(HttpServletRequest request) {
	// SessionUser su = getSessionUser(request);
	// String ucode = "";
	// if (null != su) {
	// ucode = su.getUserCode();
	// }
	// return ucode;
	// }
	//
	// public static int[] getUserRoles(HttpServletRequest request) {
	// SessionUser su = getSessionUser(request);
	// int[] rcode = null;
	// if (null != su) {
	// rcode = su.getUserRoles();
	// }
	// return rcode;
	// }
	//
	// public static int getUserId(HttpServletRequest request) {
	// SessionUser su = getSessionUser(request);
	// int uid = 0;
	// if (null != su) {
	// uid = su.getUserId();
	// }
	// return uid;
	// }
	//
	// public static String getUserName(HttpServletRequest request) {
	// SessionUser su = getSessionUser(request);
	// String uname = "";
	// if (null != su) {
	// uname = su.getUserName();
	// }
	// return uname;
	// }
	//
	// public static Date getLoginTime(HttpServletRequest request) {
	// SessionUser su = getSessionUser(request);
	// Date udate = null;
	// if (null != su) {
	// udate = su.getLoginTime();
	// }
	// return udate;
	// }
	//
	// public static String getParam(HttpServletRequest request) {
	// SessionUser su = getSessionUser(request);
	// String param = "";
	// if (null != su) {
	// param = su.getParam();
	// }
	// return param;
	// }
	//
	// public static String getDesc(HttpServletRequest request) {
	// SessionUser su = getSessionUser(request);
	// String desc = "";
	// if (null != su) {
	// desc = su.getDesc();
	// }
	// return desc;
	// }
}
