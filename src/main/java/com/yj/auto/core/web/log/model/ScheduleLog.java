package com.yj.auto.core.web.log.model;

import com.yj.auto.core.base.annotation.Table;
import com.yj.auto.core.web.log.model.bean.ScheduleLogEntity;
import com.yj.auto.core.web.system.model.Schedule;

/**
 * 任务执行日志
 */
@SuppressWarnings("serial")
@Table(name = ScheduleLog.TABLE_NAME, key = ScheduleLog.TABLE_PK, remark = ScheduleLog.TABLE_REMARK)
public class ScheduleLog extends ScheduleLogEntity<ScheduleLog> {
	private Schedule schedule;

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

}