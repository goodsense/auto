package com.yj.auto.core.web.system.model.bean;

import java.util.Date;

import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.*;

/**
 * 系统角色
 */
@SuppressWarnings("serial")
public abstract class RoleEntity<M extends RoleEntity<M>> extends BaseEntity<M> {
	public static final String TABLE_NAME = "t_sys_role"; // 数据表名称

	public static final String TABLE_PK = "id"; // 数据表主键

	public static final String TABLE_REMARK = "系统角色"; // 数据表备注

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getTableRemark() {
		return TABLE_REMARK;
	}

	public String getTablePK() {
		return TABLE_PK;
	}

	/**
	 * Column ：id
	 * 
	 * @return 角色主键
	 */

	public Integer getId() {
		return get("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * Column ：code
	 * 
	 * @return 角色代码
	 */
	@NotBlank
	@Length(max = 64)
	public String getCode() {
		return get("code");
	}

	public void setCode(String code) {
		set("code", code);
	}

	/**
	 * Column ：name
	 * 
	 * @return 角色名称
	 */
	@NotBlank
	@Length(max = 64)
	public String getName() {
		return get("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * Column ：type
	 * 
	 * @return 角色类型
	 */
	@NotBlank
	@Length(max = 64)
	public String getType() {
		return get("type");
	}

	public void setType(String type) {
		set("type", type);
	}

	/**
	 * Column ：state
	 * 
	 * @return 角色状态
	 */
	@NotBlank
	@Length(max = 32)
	public String getState() {
		return get("state");
	}

	public void setState(String state) {
		set("state", state);
	}

	/**
	 * Column ：sort
	 * 
	 * @return 排序
	 */
	@NotNull
	public Integer getSort() {
		return get("sort");
	}

	public void setSort(Integer sort) {
		set("sort", sort);
	}

	/**
	 * Column ：remark
	 * 
	 * @return 描述
	 */
	@Length(max = 1024)
	public String getRemark() {
		return get("remark");
	}

	public void setRemark(String remark) {
		set("remark", remark);
	}

	/**
	 * Column ：param1
	 * 
	 * @return 参数1
	 */
	@Length(max = 512)
	public String getParam1() {
		return get("param1");
	}

	public void setParam1(String param1) {
		set("param1", param1);
	}

	/**
	 * Column ：param2
	 * 
	 * @return 参数2
	 */
	@Length(max = 512)
	public String getParam2() {
		return get("param2");
	}

	public void setParam2(String param2) {
		set("param2", param2);
	}

	/**
	 * Column ：param3
	 * 
	 * @return 参数3
	 */
	@Length(max = 512)
	public String getParam3() {
		return get("param3");
	}

	public void setParam3(String param3) {
		set("param3", param3);
	}

	/**
	 * Column ：luser
	 * 
	 * @return 最后修改人
	 */

	public Integer getLuser() {
		return get("luser");
	}

	public void setLuser(Integer luser) {
		set("luser", luser);
	}

	/**
	 * Column ：ltime
	 * 
	 * @return 最后修改时间
	 */

	public Date getLtime() {
		return get("ltime");
	}

	public void setLtime(Date ltime) {
		set("ltime", ltime);
	}
}