package com.yj.auto.core.web.log.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.log.model.bean.*;
/**
 * 访问日志
 */
@SuppressWarnings("serial")
@Table(name = AccessLog.TABLE_NAME, key = AccessLog.TABLE_PK, remark = AccessLog.TABLE_REMARK)
public class AccessLog extends AccessLogEntity<AccessLog> {

}