package com.yj.auto.core.web.log.service;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import cn.hutool.core.date.DateUtil;
import com.yj.auto.core.base.annotation.Service;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.jfinal.base.BaseService;
import com.yj.auto.core.web.log.model.LoginLog;

/**
 * Login 管理 描述：
 */
@Service(name = "loginLogSrv")
public class LoginLogService extends BaseService<LoginLog> {

	private static final Log logger = Log.getLog(LoginLogService.class);

	public static final String SQL_LIST = "log.login.list";

	public static final LoginLog dao = new LoginLog().dao();

	@Override
	public LoginLog getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("login_time desc");
		}
		return getSqlPara(SQL_LIST, query);
	}

	public boolean updateLogout(String sessionId) {
		String sql = "update t_log_login set logout_time=? where session_id=?";
		return Db.update(sql, DateUtil.date(), sessionId) > 0;
	}

	public boolean updateLogout() {
		String sql = "update t_log_login set logout_time=? where logout_time is null";
		return Db.update(sql, DateUtil.date()) > 0;
	}

	public boolean delete(String keyword, String stime, String etime) {
		QueryModel query = new QueryModel();
		query.setKeyword(keyword);
		query.setStime(stime);
		query.setEtime(etime);
		SqlPara sql = getSqlPara("log.login.clear", query);
		return Db.update(sql) > 0;
	}
}