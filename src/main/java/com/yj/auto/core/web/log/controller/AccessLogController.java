package com.yj.auto.core.web.log.controller;

import com.jfinal.ext.kit.DateKit;
import com.jfinal.log.Log;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.base.model.ResponseModel;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.core.web.log.model.AccessLog;
import com.yj.auto.core.web.log.service.AccessLogService;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.utils.DateUtil;

/**
 * 访问日志 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "log", key = "/log/access")
public class AccessLogController extends BaseController {
	private static final Log logger = Log.getLog(AccessLogController.class);

	private static final String RESOURCE_URI = "log/access/index";
	private static final String INDEX_PAGE = "access_index.html";
	private static final String SHOW_PAGE = "access_show.html";

	AccessLogService accessLogSrv = null;

	public void index() {
		QueryModel query = new QueryModel();
		query.setStime(DateUtil.toDateStr(DateUtil.addDay(DateUtil.date(), -7)));
		setAttr("query", query);
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<AccessLog> dt = getDataTable(query, accessLogSrv);
		renderJson(dt);
	}

	public void get() {
		Integer id = getParaToInt(0);
		AccessLog model = accessLogSrv.get(id);
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(accessLogSrv);
		return res.isSuccess();
	}

	// 根据条件清空日志
	public boolean clear() {
		String keyword = getPara("keyword");
		String stime = getPara("stime");
		String etime = getPara("etime");
		boolean success = accessLogSrv.delete(keyword, stime, etime);
		ResponseModel<String> res = new ResponseModel<String>(success);
		res.setMsg("清空" + accessLogSrv.getDao().getTableRemark() + (res.isSuccess() ? "成功" : "失败"));
		renderJson(res);
		return success;
	}
}