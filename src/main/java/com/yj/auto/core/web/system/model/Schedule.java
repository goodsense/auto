package com.yj.auto.core.web.system.model;

import com.yj.auto.core.base.annotation.Table;
import com.yj.auto.core.web.system.model.bean.ScheduleEntity;

/**
 * 系统定时任务
 */
@SuppressWarnings("serial")
@Table(name = Schedule.TABLE_NAME, key = Schedule.TABLE_PK, remark = Schedule.TABLE_REMARK)
public class Schedule extends ScheduleEntity<Schedule> {
	public String getJobName() {
		return String.valueOf(getId());
	}

	public String getJobGroup() {
		return getType();
	}

	public String getTriggerName() {
		return getCode();
	}

	public String getTriggerGroup() {
		return getType();
	}
}