package com.yj.auto.core.web.system.service;

import java.io.File;
import java.util.List;

import com.jfinal.kit.FileKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.Service;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.jfinal.base.BaseService;
import com.yj.auto.core.web.system.model.Atta;
import com.yj.auto.plugin.lucene.utils.LuceneUtil;

/**
 * Atta 管理 描述：
 */
@Service(name = "attaSrv")
public class AttaService extends BaseService<Atta> {

	private static final Log logger = Log.getLog(AttaService.class);

	public static final String SQL_LIST = "system.atta.list";
	public static final String PATH_BY_DATE_FMT = "yyyyMM";
	public static final Atta dao = new Atta().dao();

	@Override
	public Atta getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("ctime desc");
		}
		return getSqlPara(SQL_LIST, query);
	}

	/**
	 * 更新附件的数据类型、数据ID
	 * 
	 * @param dataType
	 *            数据类型
	 * @param dataId
	 *            数据ID
	 * @param attaIds
	 *            附件ID
	 * @return
	 */
	public boolean updateAttaData(String dataType, Integer dataId, Integer... attaIds) {
		if (StrKit.isBlank(dataType) || null == dataId || null == attaIds || attaIds.length < 1)
			return false;
		QueryModel query = new QueryModel();
		query.setType(dataType);
		query.setId(dataId);
		query.put("idArray", attaIds);
		SqlPara sql = Db.getSqlPara("system.atta.update.data", query);
		return Db.update(sql) > 0;
	}

	public boolean delete(Atta model) {
		if (null == model)
			return false;
		// 删除硬盘文件
		String path = model.getPath();
		File file = new File(model.getAbsolutePath());
		FileKit.delete(file);
		try {// 删除索引
			LuceneUtil.delDocument(model);
		} catch (Exception e) {
		}
		return model.delete();
	}

	public boolean delete(Integer... ids) {
		if (null == ids || ids.length < 1)
			return false;
		boolean success = true;
		for (Integer id : ids) {
			Atta model = get(id);
			success = delete(model);
		}
		return success;
	}

	/**
	 * 根据数据类型、数据ID删除附件
	 * 
	 * @param dataType
	 *            数据类型
	 * @param dataId
	 *            数据ID
	 * @return
	 */
	public boolean delete(String dataType, Integer dataId) {
		List<Atta> list = findByDatas(dataType, dataId);
		boolean success = true;
		for (Atta model : list) {
			// success = success && delete(model);真是奇怪，这样如果有一个是返回false接下去就不执行了
			success = delete(model);
		}
		return success;
	}

	// 根据数据类型与业务主键，获取附件
	public List<Atta> findByDatas(String dataType, Integer dataId) {
		QueryModel query = new QueryModel();
		query.setType(dataType);
		query.setId(dataId);
		query.setOrderby("ctime");
		SqlPara sql = getListSqlPara(query);
		List<Atta> list = find(sql);
		return list;
	}

	// 根据数据类型与业务主键，获取第一个附件
	public Atta getByData(String dataType, Integer dataId) {
		List<Atta> list = findByDatas(dataType, dataId);
		if (null != list && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	// 五小时未设置data_id的文件认为是无效附件
	public List<Atta> findInvalidAttas() {
		String sql = "select * from t_sys_atta where data_id is null and ctime<date_add(now(),interval -1 hour)";
		List<Atta> list = getDao().find(sql);
		return list;
	}

	// 删除无效附件
	public void deleteInvalidAttas() {
		List<Atta> list = findInvalidAttas();
		for (Atta model : list) {
			delete(model);
		}
	}
}