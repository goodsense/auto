package com.yj.auto.utils;

import java.net.InetAddress;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.conn.util.InetAddressUtils;

public class NetUtil extends cn.hutool.core.util.NetUtil{

	public static InetAddress[] getInetAddress() {
		InetAddress all[] = null;
		try {
			InetAddress inetaddress = NetUtil.getLocalhost();
			if (null != inetaddress) {
				all = InetAddress.getAllByName(inetaddress.getHostName());
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return all;
	}

	public static String[] getAllMAC() throws Exception {
		Set<String> set = new HashSet<String>();
		InetAddress[] list = getInetAddress();
		for (InetAddress add : list) {
			if (add.isLoopbackAddress() || !InetAddressUtils.isIPv4Address(add.getHostAddress()))
				continue;
			String mac = NetUtil.getMacAddress(add);
			set.add(mac);

		}
		return (String[]) set.toArray(new String[set.size()]);
	}

	// // 获取MAC地址的方法
	// public static String getMACAddress() throws Exception {
	// return NetUtil.getLocalMacAddress();
	// }

	// // 获取MAC地址的方法
	// public static String getMACAddress(InetAddress ia) throws Exception {
	// // 获得网络接口对象（即网卡），并得到mac地址，mac地址存在于一个byte数组中。
	// byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
	// // 下面代码是把mac地址拼装成String
	// StringBuffer sb = new StringBuffer();
	// for (int i = 0; i < mac.length; i++) {
	// if (i != 0) {
	// sb.append("-");
	// }
	// // mac[i] & 0xFF 是为了把byte转化为正整数
	// String s = Integer.toHexString(mac[i] & 0xFF);
	// sb.append(s.length() == 1 ? 0 + s : s);
	// }
	// // 把字符串所有小写字母改为大写成为正规的mac地址并返回
	// return sb.toString().toUpperCase();
	// }

	/**
	 * 当有apach、array等反向代理架构时，由于在客户端和服务之间增加了中间层，因此服务器无法直接拿到客户端的IP，
	 * 服务器端应用也无法直接通过转发请求的地址返回给客户端。
	 * 但是在转发请求的HTTP头信息中，增加了X－FORWARDED－FOR信息。用以跟踪原有的客户端IP地址和原来客户端请求的服务器地址。
	 * 
	 * @param request
	 * @return 客户端真实IP
	 */
	public static String getRemoteAddr(HttpServletRequest request) {
		if (request == null) {
			return "";
		}
		// 如果有apache、array等反向代理架构
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || "".equals(ip)) {
			// 如果有weblogic配置
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || "".equals(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static String getRequestUri(HttpServletRequest request) {
		String uri = null;
		if (null != request) {
			uri = request.getRequestURI();
		}
		return uri;
	}

	public static String getCookieString(HttpServletRequest request, String cookieName) {
		String cookieId = "";// 存储JSESSION
		Cookie cookies[] = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				if (cookieName.equals(cookie.getName())) {
					cookieId = cookieName + ":" + cookie.getValue();
				}
			}
		}
		return cookieId;
	}

	public static boolean isAjax(HttpServletRequest request) {
		if (request != null && "XMLHttpRequest".equalsIgnoreCase(request.getHeader("x-requested-with"))) {// ajax请求
			return true;
		}
		return false;
	}
}
