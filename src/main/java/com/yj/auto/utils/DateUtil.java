package com.yj.auto.utils;

import java.util.Calendar;
import java.util.Date;

import com.jfinal.ext.kit.DateKit;

public class DateUtil extends cn.hutool.core.date.DateUtil {
	public static Calendar getCalendar() {
		return Calendar.getInstance();
	}

	public static int getYear() {
		return getCalendar().get(Calendar.YEAR);
	}

	public static int getMonth() {
		return getCalendar().get(Calendar.MONTH) + 1;
	}

	public static int getDay4Month() {
		return getCalendar().get(Calendar.DAY_OF_MONTH) + 1;
	}

	public static Date addDay(Date date, int day) {
		if (null == date)
			return null;
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, day);
		return c.getTime();
	}

	public static String toDateStr(Date date) {
		if (null == date)
			return "";
		return DateKit.toStr(date);
	}

	public static String toTimeStr(Date date) {
		if (null == date)
			return "";
		return DateKit.toStr(date, DateKit.timeStampPattern);
	}
}
