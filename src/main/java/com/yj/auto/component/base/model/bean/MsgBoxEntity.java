package com.yj.auto.component.base.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;
import com.yj.auto.core.jfinal.base.*;

/**
 *  系统消息文件夹
 */
@SuppressWarnings("serial")
public abstract class MsgBoxEntity<M extends MsgBoxEntity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "t_com_msg_box"; //数据表名称
	
	public static final String TABLE_PK = "id"; //数据表主键
	
	public static final String TABLE_REMARK = "系统消息文件夹"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   		
	/**
	 * Column ：id
	 * @return 文件夹主键
	 */
   		
	public Integer getId(){
   		return get("id");
   	}
	
	public void setId(Integer id){
   		set("id" , id);
   	}	
   		
	/**
	 * Column ：type
	 * @return 队列类别：01=收件箱；02=发件箱；03=草稿箱
	 */
   	@NotBlank

	@Length(max = 32)	
	public String getType(){
   		return get("type");
   	}
	
	public void setType(String type){
   		set("type" , type);
   	}	
   		
	/**
	 * Column ：msg_id
	 * @return 消息主键
	 */
   	@NotNull 	
	public Integer getMsgId(){
   		return get("msg_id");
   	}
	
	public void setMsgId(Integer msgId){
   		set("msg_id" , msgId);
   	}	
   		
	/**
	 * Column ：user_id
	 * @return 文件夹拥有者
	 */
   	@NotNull 	
	public Integer getUserId(){
   		return get("user_id");
   	}
	
	public void setUserId(Integer userId){
   		set("user_id" , userId);
   	}	
   		
	/**
	 * Column ：read_time
	 * @return 首次阅读时间
	 */
   		
	public Date getReadTime(){
   		return get("read_time");
   	}
	
	public void setReadTime(Date readTime){
   		set("read_time" , readTime);
   	}	
   		
	/**
	 * Column ：receipt_time
	 * @return 回执时间
	 */
   		
	public Date getReceiptTime(){
   		return get("receipt_time");
   	}
	
	public void setReceiptTime(Date receiptTime){
   		set("receipt_time" , receiptTime);
   	}	
   		
	/**
	 * Column ：param1
	 * @return 参数1
	 */
   	@Length(max = 512)	
	public String getParam1(){
   		return get("param1");
   	}
	
	public void setParam1(String param1){
   		set("param1" , param1);
   	}	
   		
	/**
	 * Column ：param2
	 * @return 参数2
	 */
   	@Length(max = 512)	
	public String getParam2(){
   		return get("param2");
   	}
	
	public void setParam2(String param2){
   		set("param2" , param2);
   	}	
   		
	/**
	 * Column ：param3
	 * @return 参数3
	 */
   	@Length(max = 512)	
	public String getParam3(){
   		return get("param3");
   	}
	
	public void setParam3(String param3){
   		set("param3" , param3);
   	}	
}